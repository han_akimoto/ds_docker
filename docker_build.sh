#!/bin/sh
#echo $0
BUILD_DIR=`dirname "$0"`

docker-machine.exe start default

docker-machine.exe ssh default "sudo mkdir /data"

docker-machine.exe ssh default "sudo mount data /data -t vboxsf -o uid=0,gid=0,fmode=0777,dmode=0777"