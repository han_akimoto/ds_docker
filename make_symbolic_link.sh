#!/bin/bash

##################################################
# シンボリックリンクの作成を行う
# 必ずwebapp配下で行ってください 
##################################################

##
# ヘルプメッセージ
##
display_help() {
    echo ""
    echo " example:"
    echo "    sh tools_ext/data_setup/make_symbolic_link.sh /opt/ds2core-fvfox-05.01" 
    echo ""
    exit -1
}

##
# オプションを取得します
##
CORE_DIR=$1
CURR_DIR=$(pwd)
echo 'start...'
echo $CURR_DIR

cd $CURR_DIR/apps/
backend=$(pwd)
echo $backend
if test -d ./backend
then rm -rf ./backend
echo "hogehgehogehoge"
fi
ln -sf $CORE_DIR/apps/backend ./backend

cd $CURR_DIR
if test -d ./lib
then rm -rf lib
fi
ln -sf $CORE_DIR/lib/ ./lib

if test -d ./tools
then rm -rf tools
fi
ln -sf $CORE_DIR/tools/ ./tools

if test -d ./batch_common
then rm -rf batch_common
fi
ln -sf $CORE_DIR/batch_common/ ./batch_common

if test -d ./plugins
then echo ''
else mkdir ./plugins/
fi

cd $CURR_DIR/plugins
plugins=$(pwd)
echo $plugins
if test -d ./dinoAutoTrimmingFilterPlugin
then rm -rf ./dinoAutoTrimmingFilterPlugin
fi
ln -sf $CORE_DIR/plugins/dinoAutoTrimmingFilterPlugin ./dinoAutoTrimmingFilterPlugin

if test -d ./dinoMailMagazinePlugin
then rm -rf ./dinoMailMagazinePlugin
fi
ln -sf $CORE_DIR/plugins/dinoMailMagazinePlugin ./dinoMailMagazinePlugin

if test -d ./dinoMailPlugin
then rm -rf ./dinoMailPlugin
fi
ln -sf $CORE_DIR/plugins/dinoMailPlugin ./dinoMailPlugin

if test -d ./dinoPropelLoadbalancePlugin
then rm -rf ./dinoPropelLoadbalancePlugin
fi
ln -sf $CORE_DIR/plugins/dinoPropelLoadbalancePlugin ./dinoPropelLoadbalancePlugin

if test -d ./ds2CorePointSystemPlugin
then rm -rf ./ds2CorePointSystemPlugin
fi
ln -sf $CORE_DIR/plugins/ds2CorePointSystemPlugin ./ds2CorePointSystemPlugin

if test -d ./ds2FrontCoreModulesPlugin
then rm -rf ./ds2FrontCoreModulesPlugin
fi
ln -sf $CORE_DIR/plugins/ds2FrontCoreModulesPlugin ./ds2FrontCoreModulesPlugin

if test -d ./ds2PointSystemPlugin
then rm -rf ./ds2PointSystemPlugin
fi
ln -sf $CORE_DIR/plugins/ds2PointSystemPlugin ./ds2PointSystemPlugin

if test -d ./ds2NoveltyPlugin
then rm -rf ./ds2NoveltyPlugin
fi
ln -sf $CORE_DIR/plugins/ds2NoveltyPlugin ./ds2NoveltyPlugin

if test -d ./ds2PropelAuditBehaviorPlugin
then rm -rf ./ds2PropelAuditBehaviorPlugin
fi
ln -sf $CORE_DIR/plugins/ds2PropelAuditBehaviorPlugin ./ds2PropelAuditBehaviorPlugin

if test -d ./ds2SiteCatalystPlugin
then rm -rf ./ds2SiteCatalystPlugin
fi
ln -sf $CORE_DIR/plugins/ds2SiteCatalystPlugin ./ds2SiteCatalystPlugin

if test -d ./sfAdvancedLoggerPlugin
then rm -rf ./sfAdvancedLoggerPlugin
fi
ln -sf $CORE_DIR/plugins/sfAdvancedLoggerPlugin ./sfAdvancedLoggerPlugin

if test -d ./sfConfigurePlugin
then rm -rf ./sfConfigurePlugin
fi
ln -sf $CORE_DIR/plugins/sfConfigurePlugin ./sfConfigurePlugin

if test -d ./sfCSRFPlugin
then rm -rf ./sfCSRFPlugin
fi
ln -sf $CORE_DIR/plugins/sfCSRFPlugin ./sfCSRFPlugin

if test -d ./sfJqueryReloadedPlugin
then rm -rf ./sfJqueryReloadedPlugin
fi
ln -sf $CORE_DIR/plugins/sfJqueryReloadedPlugin ./sfJqueryReloadedPlugin

if test -d ./sfLabelHelperGeneratorPlugin
then rm -rf ./sfLabelHelperGeneratorPlugin
fi
ln -sf $CORE_DIR/plugins/sfLabelHelperGeneratorPlugin ./sfLabelHelperGeneratorPlugin

if test -d ./sfMobileCarrierJPPlugin
then rm -rf ./sfMobileCarrierJPPlugin
fi
ln -sf $CORE_DIR/plugins/sfMobileCarrierJPPlugin ./sfMobileCarrierJPPlugin

if test -d ./sfPropelRemainPlugin
then rm -rf ./sfPropelRemainPlugin
fi
ln -sf $CORE_DIR/plugins/sfPropelRemainPlugin ./sfPropelRemainPlugin

if test -d ./sfSchemaBeautifierPlugin
then rm -rf ./sfSchemaBeautifierPlugin
fi
ln -sf $CORE_DIR/plugins/sfSchemaBeautifierPlugin ./sfSchemaBeautifierPlugin

if test -d ./sfSslRequirementPlugin
then rm -rf ./sfSslRequirementPlugin
fi
ln -sf $CORE_DIR/plugins/sfSslRequirementPlugin ./sfSslRequirementPlugin

if test -d ./sfTCPDFPlugin
then rm -rf ./sfTCPDFPlugin
fi
ln -sf $CORE_DIR/plugins/sfTCPDFPlugin ./sfTCPDFPlugin

if test -d ./sfThumbnailPlugin
then rm -rf ./sfThumbnailPlugin
fi
ln -sf $CORE_DIR/plugins/sfThumbnailPlugin ./sfThumbnailPlugin

if test -d ./sfWebBrowserPlugin
then rm -rf ./sfWebBrowserPlugin
fi
ln -sf $CORE_DIR/plugins/sfWebBrowserPlugin ./sfWebBrowserPlugin

if test -d ./ds2GiftLibraryPlugin
then rm -rf ./ds2GiftLibraryPlugin
fi
ln -sf $CORE_DIR/plugins/ds2GiftLibraryPlugin ./ds2GiftLibraryPlugin 

if test -d ./ds2StockControlPlugin
then rm -rf ./ds2StockControlPlugin
fi
ln -sf $CORE_DIR/plugins/ds2StockControlPlugin ./ds2StockControlPlugin

if test -d ./ds2CoreCrossPointPlugin
then rm -rf ./ds2CoreCrossPointPlugin
fi
ln -sf $CORE_DIR/plugins/ds2CoreCrossPointPlugin ./ds2CoreCrossPointPlugin

cd $CURR_DIR/data/
config=$(pwd)
echo $config
if test -d ./config
then rm -rf config
fi
ln -sf $CORE_DIR/data/config/ ./config

if test -d ./schema
then rm -rf schema
fi
ln -sf $CORE_DIR/data/schema/ ./schema

echo '...end'
