MYSQLUSER=root # MYSQLのユーザー名
MYSQLPASSWORD=password # MYSQLのパスワード
MYSQLHOST=172.18.0.2 # 自分のローカルDBのIPアドレス

DBNAME=`echo $1 |sed "s|/$||"`
DBNAME_DEST=`echo $2 |sed "s|/$||"`

if [ -z $DBNAME_DEST ]; then
  DBNAME_DEST=$DBNAME
fi

die() {
  echo "something wrong.." 1>&2
  exit
}

if [ -f $DBNAME.mysqldump.gz ]; then
  rm $DBNAME.mysqldump.gz
fi

wget http://ds1dev:ut78i\$p@stgmente.dh-ds1.aloha-inc.jp/aws_db_dump/$DBNAME.mysqldump.gz || die
gzip -d $DBNAME.mysqldump.gz
grep -v 'DEFINER' $DBNAME.mysqldump > $DBNAME.NODEFINER.mysqldump || die
rm $DBNAME.mysqldump
echo "DROP DATABASE IF EXISTS $DBNAME_DEST" | mysql --default-character-set=utf8 -h $MYSQLHOST -u $MYSQLUSER --password=$MYSQLPASSWORD
echo "CREATE DATABASE $DBNAME_DEST DEFAULT CHARACTER SET utf8" | mysql --default-character-set=utf8 -h $MYSQLHOST -u $MYSQLUSER --password=$MYSQLPASSWORD || die
#echo "CREATE TABLES $DBNAME_DEST MYSQLDUMP" | mysql --default-character-set=utf8 -h $MYSQLHOST -u $MYSQLUSER --password=$MYSQLPASSWORD $DBNAME_DEST < $DBNAME.NODEFINER.mysqldump || die
echo "CREATE TABLES $DBNAME_DEST MYSQLDUMP" | pv $DBNAME.NODEFINER.mysqldump | mysql --default-character-set=utf8 -h $MYSQLHOST -u $MYSQLUSER --password=$MYSQLPASSWORD $DBNAME_DEST || die
rm $DBNAME.NODEFINER.mysqldump